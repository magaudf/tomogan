import matplotlib.pyplot as plt
import numpy as np

f = open("../tf2/depth3-itrOut/iter-prints.log", "r")
files = f.readlines()
gloss = []
dloss= []

for i in files:
	g = (i.split(",")[1]).split(":")[1]
	d = (i.split(",")[6]).split(":")[1]
	gloss.append(float(g))
	dloss.append(float(d))

plt.figure(figsize=(15, 10))
plt.subplot(121)
plt.plot(gloss)
plt.title('Loss function Generator', fontsize=18)
plt.subplot(122)
plt.plot(dloss)
plt.title('Loss function Discriminator', fontsize=18)


plt.savefig('plot.png')
