Affichage

Permet d'afficher les fonctions de loss du générateur et du dscriminateur.

Save_image

Enregistre l'image bruité l'image non bruité et l'image débruité sous forme d'un png (enregistre un plot des 3 images),
avec une colorbar et une scale bar.

Transfo_tiff_hdf5

Transforme deux fichiers image tiff, un contenant les images bruitées et le second contenant les images clean en un fichier hdf5
en séparant une partie test et une partie train.
