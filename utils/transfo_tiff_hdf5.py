import numpy as np
import h5py, os
from PIL import Image


noisy_path = ""		
cleane_path = ""	
save_path = ""

ns_image = Image.open(noisy_path)
gt_image = Image.open(cleane_path)

nsimages = []
gtimages = []
h,l=ns_image.size
h = (h//8)*8
l = (l//8)*8

for i in range(ns_image.n_frames):
    ns_image.seek(i)
    nsimages.append(np.array(ns_image))
    gt_image.seek(i)
    gtimages.append(np.array(gt_image))

ns_image = np.asarray(nsimages)
gt_image = np.asarray(gtimages)
nb_image = ns_image.shape[0]
nb_train = int(ns_image.shape[0]*0.8)

# tronque les images au multiples de 8 le plus proche
ns_image_train = ns_image[0:nb_train,0:l,0:h]
gt_image_train = gt_image[0:nb_train,0:l,0:h]

ns_image_test = ns_image[nb_train:nb_image,0:l,0:h]
gt_image_test = gt_image[nb_train:nb_image,0:l,0:h]

print(gt_image_test.shape)
# sauvegarde 
with h5py.File(save_path, "w") as f:
    dset1 = f.create_dataset("test_gt", data = gt_image_test)
    dset2 = f.create_dataset("test_ns", data = ns_image_test)
    dset3 = f.create_dataset("train_gt", data=gt_image_train)
    dset4 = f.create_dataset("train_ns", data=ns_image_train)
f.close()




