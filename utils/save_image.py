import sys, os, time, argparse, shutil, scipy, h5py, glob
import matplotlib.pyplot as plt
import numpy as np
from matplotlib_scalebar.scalebar import ScaleBar

path = "../resultat/new_data_3_12k.h5"

pixel_size = 3
unite = "um"

with h5py.File(path, 'r') as h5fd:
    ns_img = h5fd['ns'][86,:,:]
    gt_img = h5fd['gt'][86,:,:]
    dn_img = h5fd['dn'][86,:,:]
    
plt.figure(figsize=(15, 10))
ax1 = plt.subplot(131)
plt.imshow(ns_img, cmap='gray')
plt.colorbar(shrink=0.4)
scalebar = ScaleBar(pixel_size, unite, length_fraction=0.25,box_alpha = 0.1)
ax1.add_artist(scalebar)
plt.title('Noisy/Input', fontsize=18)
ax2 = plt.subplot(132)
plt.imshow(gt_img, cmap='gray')
plt.colorbar(shrink=0.4,ax=ax2)
scalebar = ScaleBar(pixel_size, unite, length_fraction=0.25,box_alpha = 0.1)
ax2.add_artist(scalebar)
plt.title('Clean/Label', fontsize=18)
ax3 = plt.subplot(133)
plt.imshow(dn_img, cmap='gray')
plt.colorbar(shrink=0.4,ax=ax3)
scalebar = ScaleBar(pixel_size, unite, length_fraction=0.25,box_alpha = 0.1)
ax3.add_artist(scalebar)
plt.title('Denoised/output', fontsize=18)
plt.tight_layout();
plt.savefig('plot.png')
