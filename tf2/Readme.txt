main-gan

Fichier utilisé pour l'entrainement du réseaux de neuronnes, Les images doiven,t avoirs une taille multiples de 8 
(due au max pooling et upsampling)

il a un parametres obligatoire :

-dsfn : path vers le fichier hdf5 contenant les données sous le bon format

Parametres optionel :

-expName : nom du fichier dans lequel serta stocké les résultats de l'entrainement
-depth : nombre de slice adjacente utilisé pour l'entrainement (défaut = 1)
-psz : taille des sous-images utilisé pour l'augmentation de données (défaut = 256)
-mbsz : nombre de sous images créé lors de l'augmentation de données
-maxiter : nombre d'itérations à faire
-print : affichage des logs (défaut = False, 1 affiche dans le terminal les logs, 0 redirige les logs dans un fichier)

example :

python main-gan.py -gpu=0 -expName=test -dsfn=dataset/demo-dataset-real.h5

Infer

Fichier permettant de faire les prédictions

il a deux parametres obligatoire :

-dsfn : path vers le fichier hdf5 contenant les données sous le bon format
-mdl : path vers le fichiers hdf5 contenant le modele

Parametre optionel :

-depth : nombre de slice adjacente utilisé pour l'entrainement (défaut = 1)
